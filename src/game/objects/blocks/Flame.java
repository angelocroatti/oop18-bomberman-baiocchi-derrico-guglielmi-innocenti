package game.objects.blocks;

import java.awt.Graphics2D;
import java.util.stream.Collectors;

import game.graphics.AliveEntityGraphicsComponent;
import game.graphics.GraphicsComponent;
import game.object.Solidity;
import game.objects.entities.AliveObject;
import game.physics.FlamePhysicsComponent;
import game.theme.FlameSprites;
import game.utilities.Position;
import game.world.World;

/**
 * This class represents the {@link Flame}, with its components.
 */
public class Flame extends AbstractStillObject implements AliveObject {

    private static final int FLAME_LIFE_TIME = 200;

    private final GraphicsComponent graphicComponent;
    private final FlamePhysicsComponent physicsComponent;
    private final Bomb bomb;
    private int elapsedTime;

    /**
     * Creates the {@code Flame} entity.
     *
     * @param position the {@link Flame} {@link Position}.
     * @param flameSprite the {@link FlameSprites}.
     * @param bomb the game {@link Bomb}.
     */
    public Flame(final Position position, final FlameSprites flameSprite, final Bomb bomb) {
        super(Solidity.UNBREAKABLE, position);
        this.graphicComponent = new AliveEntityGraphicsComponent(flameSprite, this);
        this.physicsComponent = new FlamePhysicsComponent(this);
        this.bomb = bomb;
        this.elapsedTime = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        this.graphicComponent.render(g);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(final long elapsedTime) {
        final World world = ((Bomb) this.bomb).getWorld();
        this.elapsedTime += elapsedTime;
        if (this.elapsedTime >= FLAME_LIFE_TIME) {
            this.physicsComponent.checksCollisions(world.getAllGameObjects().stream().collect(Collectors.toSet()));
            world.removeObject(this);
        }
    }

    /**
     * Gets the {@link Bomb}.
     *
     * @return the {@link Bomb}
     */
    public Bomb getBomb() {
        return this.bomb;
    }

}
