package game.objects.blocks;

import java.awt.Graphics2D;

import game.graphics.AliveEntityGraphicsComponent;
import game.graphics.GraphicsComponent;
import game.object.Solidity;
import game.objects.entities.AliveObject;
import game.physics.BombPhysicsComponent;
import game.theme.BombSprites;
import game.utilities.Position;
import game.world.World;
import sounds.Sound;

/**
 * This class represents the {@link Bomb}, with its components.
 */
public class Bomb extends AbstractStillObject implements AliveObject {

    private static final int BOMB_LIFE_TIME = 3000;

    private final GraphicsComponent graphicComponent;
    private final BombPhysicsComponent physicsComponent;
    private final World world;
    private final Sound explosionSound;
    private int elapsedTime;

    /**
     * Creates the {@code Bomb} entity.
     *
     * @param position the bomb {@link Position}.
     * @param bombSprite the {@link BombSprites}.
     * @param world the game {@link World}.
     */
    public Bomb(final Position position, final BombSprites bombSprite, final World world) {
        super(Solidity.BREAKABLE, position);
        this.graphicComponent = new AliveEntityGraphicsComponent(bombSprite, this);
        this.physicsComponent = new BombPhysicsComponent(this);
        this.world = world;
        this.explosionSound = this.world.getTheme().getSounds().getExplosionSound();
        this.elapsedTime = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        this.graphicComponent.render(g);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(final long elapsedTime) {
        this.elapsedTime += elapsedTime;
        if (this.elapsedTime >= BOMB_LIFE_TIME) {
            this.physicsComponent.explosion(this.world);
            this.explosionSound.play();
        }
    }

    /**
     * Gets the {@link World}.
     *
     * @return the {@link World}
     */
    public World getWorld() {
        return this.world;
    }

    /**
     * Gets the {@link BombPhysicsComponent}.
     *
     * @return the {@link BombPhysicsComponent}
     */
    public BombPhysicsComponent getPhysicsComponent() {
        return this.physicsComponent;
    }

}
