package game.objects.blocks;

import java.awt.Graphics2D;

import game.graphics.DoorGraphicComponent;
import game.graphics.GraphicsComponent;
import game.object.Solidity;
import game.theme.DoorSprite;
import game.utilities.Position;
import sounds.Sound;

/**
 * This class manages a simple door.
 * It extends {@link AbstractStillObject}.
 */
public class Door extends AbstractStillObject {

    private boolean unlocked;
    private final GraphicsComponent graphicComponent;
    private final Sound openDoorSound;

    /**
     * Creates a {@code Door}.
     * 
     * @param position door's position
     * @param doorSprite door's sprite
     * @param openDoorSound opening door sound
     */
    public Door(final Position position, final DoorSprite doorSprite, final Sound openDoorSound) {
        super(Solidity.UNBREAKABLE, position);
        this.unlocked = false;
        this.graphicComponent = new DoorGraphicComponent(this, doorSprite);
        this.openDoorSound = openDoorSound;
    }

    /**
     * Opens the door.
     */
    public void open() {
        this.unlocked = true;
        this.openDoorSound.play();
    }

    /**
     * Closes the door.
     */
    public void close() {
        this.unlocked = false;
    }

    /**
     * Gets the field open.
     * 
     * @return true if the door is open, otherwise false
     */
    public boolean isOpen() {
        return this.unlocked;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        this.graphicComponent.render(g);
    }

}
