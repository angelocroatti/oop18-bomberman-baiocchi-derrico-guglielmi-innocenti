package game.objects.entities;

import game.object.AbstractGameObject;
import game.object.Solidity;
import game.utilities.Position;
import game.world.World;

/**
 * This class is an abstract implementation of {@link DynamicObject}.
 * It defines a generic dynamic game entity.
 */
public abstract class AbstractEntity extends AbstractGameObject implements DynamicObject {

    private final World world;

    /**
     * Creates an {@code AbstractEntity}.
     * 
     * @param position initial entity
     * @param breakable is a boolean value that says if entity is breakable or not
     * @param world reference 
     */
    public AbstractEntity(final Position position, final Solidity breakable, final World world) {
        super(breakable, position);
        this.world = world;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public World getWorld() {
        return this.world;
    }

}
