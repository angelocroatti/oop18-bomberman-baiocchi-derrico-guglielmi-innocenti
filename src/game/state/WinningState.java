package game.state;

import javax.swing.JFrame;

import menu.frame.GameMenuWinning;

/**
 * The {@link GameState} that's run the {@link GameMenuWinning}.
 */
public class WinningState implements GameState {

    private final JFrame menuWinning;

    /**
     * Creates a {@code WinningState}.
     * 
     * @param gameContext the {@link GameContext} handler.
     */
    public WinningState(final GameContext gameContext) {
        this.menuWinning = new GameMenuWinning(gameContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runState() {
        this.menuWinning.validate();
        this.menuWinning.setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeFrameState() {
        this.menuWinning.dispose();
    }

}
