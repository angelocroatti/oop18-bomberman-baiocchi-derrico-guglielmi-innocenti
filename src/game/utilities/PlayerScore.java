/**
 * 
 */
package game.utilities;

import java.io.Serializable;

/**
 * An interface to managed the player score.
 */
public interface PlayerScore extends Serializable {

    /**
     * Gets the actual score.
     * 
     * @return the actual score.
     */
    int getScore();

    /**
     * Increases or decreases the score.
     * 
     * @param score the score to be added.
     */
    void addScore(int score);

}
