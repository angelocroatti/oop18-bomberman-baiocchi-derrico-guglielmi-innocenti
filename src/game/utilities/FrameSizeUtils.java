package game.utilities;

import java.awt.Toolkit;

/**
 * Utility class to define the tiles number per edge, and get the correct
 * edge length of the game frame.
 */
public final class FrameSizeUtils {

    private static final int EDGE_LENGTH = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2
                                         - (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 10);
    /**
     * Number of tiles for any edge.
     */
    public static final int NUM_TILES = 15;

    private FrameSizeUtils() {
    }

    /**
     * Gets the correct edge length according to the number of tiles.
     *
     * @return the correct edge length
     */
    public static int getEdgeLength() {
        final int rest = EDGE_LENGTH % NUM_TILES;
        if (rest != 0) {
            return EDGE_LENGTH - rest;
        }
        return EDGE_LENGTH;
    }

}
