package game.theme;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import game.graphics.Sprite;
import game.graphics.SpriteSheet;

/**
 * This class stores any flame {@link Sprite}.
 */
public class FlameSprites extends AbstractAliveEntitySprites {

    private static final int Y_LOCATION = 12;
    private static final int N_SPRITES = 7;

    /**
     * Creates {@code FlameSprites} container.
     *
     * @param sheet {@link SpriteSheet} where to take the {@link Sprite}
     */
    public FlameSprites(final SpriteSheet sheet) {
        super();
        IntStream.range(0, N_SPRITES).mapToObj(a -> new Sprite(sheet, a + 1, Y_LOCATION))
                                     .collect(Collectors.toList())
                                     .forEach(a -> super.getSprites().add(a));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSpritesNumber() {
        return N_SPRITES;
    }

}
