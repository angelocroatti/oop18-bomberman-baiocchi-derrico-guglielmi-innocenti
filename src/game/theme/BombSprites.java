package game.theme;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import game.graphics.Sprite;
import game.graphics.SpriteSheet;

/**
 * This class stores any bomb {@link Sprite}.
 */
public class BombSprites extends AbstractAliveEntitySprites {

    private static final int Y_LOCATION = 2;
    private static final int STARTING_X_LOCATION = 3;
    private static final int N_SPRITES = 4;

    /**
     * Creates a {@code BombSprites} container.
     *
     * @param sheet {@link SpriteSheet} where to take the {@link Sprite}
     */
    public BombSprites(final SpriteSheet sheet) {
        super();
        IntStream.range(0, N_SPRITES).mapToObj(a -> new Sprite(sheet, a + STARTING_X_LOCATION, Y_LOCATION))
                                     .collect(Collectors.toList())
                                     .forEach(a -> super.getSprites().add(a));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getSpritesNumber() {
        return N_SPRITES;
    }

}
