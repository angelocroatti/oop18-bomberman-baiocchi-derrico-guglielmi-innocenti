package game.theme;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * This class stores any sound and any sprite about the game theme.
 * It takes a folder to specify which theme to choose.
 */
public class ThemeImpl implements Theme {

    private final SoundsManager sounds;
    private final SpritesManager sprites;

    /**
     * Creates a {@code ThemeImpl} that initialize the {@link SpritesManager} and {@link SoundsManager}.
     *
     * @param folder that selects which theme to load
     * @throws UnsupportedAudioFileException wrong audio file format
     * @throws IOException problem during input/output
     * @throws LineUnavailableException audio line can't be opened because it is unavailable
     */
    public ThemeImpl(final String folder) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        this.sounds = new SoundsManager(folder);
        this.sprites = new SpritesManager(folder);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SoundsManager getSounds() {
        return this.sounds;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpritesManager getSprites() {
        return this.sprites;
    }

}
