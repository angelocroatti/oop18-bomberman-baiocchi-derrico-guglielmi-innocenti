package game.maps;

import java.util.stream.Collectors;

import game.graphics.SpriteSheet;
import game.utilities.FrameSizeUtils;
import game.world.GameObjectFactory;

/**
 * It represents a {@link GameMap} where blocks are placed horizontally.
 */
public class HorizontalMap extends AbstractGameMap {

    private static final int SPRITE_SIZE = SpriteSheet.SPRITE_SIZE_IN_GAME;
    private static final int NUM_TILES = FrameSizeUtils.NUM_TILES;
    private static final int EVEN_INDEX = 2;

    /**
     * Creates an {@code HorizontalMap} composed by blocks with same distance between them and
     * disposed horizontally.
     *
     * @param factory {@link GameObjectFactory} to create blocks
     */
    public HorizontalMap(final GameObjectFactory factory) {
        super(factory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initializeMapLayout(final GameObjectFactory factory) {
        super.getMapLayout().addAll(super.getFreePositions().stream()
                            .filter(p -> (p.getY() / SPRITE_SIZE) >= EVEN_INDEX
                                          && (p.getY() / SPRITE_SIZE) < NUM_TILES - EVEN_INDEX
                                          && (p.getX() / SPRITE_SIZE) % EVEN_INDEX == 0)
                            .map(a -> factory.createUnbreakableBlock(a))
                            .collect(Collectors.toSet()));
    }
}
