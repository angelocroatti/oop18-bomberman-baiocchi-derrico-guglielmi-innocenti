package game.world;

import game.objects.blocks.Block;
import game.objects.blocks.Door;
import game.objects.blocks.PickableObject;
import game.objects.entities.BalloomEnemy;
import game.objects.entities.GhostEnemy;
import game.objects.entities.PlayerImpl;
import game.objects.entities.SmartEnemy;
import game.utilities.Position;

/**
 * This interface declares the method of a GameObjectFactory.
 */
public interface GameObjectFactory {

    /**
     * Creates a {@link PlayerImpl}.
     * 
     * @param position player's position.
     * @param world the world where to add the player
     * @return a new player
     */
    PlayerImpl createPlayer(Position position, World world);

    /**
     * Creates a {@link BalloomEnemy}.
     * 
     * @param position enemy's position
     * @param world the world where to add the balloom enemy
     * @return a new BalloomEnemy
     */
    BalloomEnemy createBalloomEnemy(Position position, World world);

    /**
     * Creates a {@link GhostEnemy}.
     * 
     * @param position enemy's position
     * @param world the world where to add the ghost enemy
     * @return a new GhostEnemy
     */
    GhostEnemy createGhostEnemy(Position position, World world);

    /**
     * Creates a {@link SmartEnemy}.
     * 
     * @param position enemy's position
     * @param world the world where to add the smart enemy
     * @return a new SmartEnemy
     */
    SmartEnemy createSmartEnemy(Position position, World world);

    /**
     * Creates an unbreakable {@link Block}.
     * 
     * @param position block's position
     * @return a new unbreakable Block
     */
    Block createUnbreakableBlock(Position position);

    /**
     * Creates a breakable {@link Block}.
     * 
     * @param position block's position
     * @return a new breakable Block
     */
    Block createBreakableBlock(Position position);

    /**
     * Creates a {@link Door}.
     * 
     * @param position door's position
     * @return a new Door
     */
    Door createDoor(Position position);

    /**
     * Creates a {@link PickableObject} of type key.
     * 
     * @param position key's position
     * @return a new Key
     */
    PickableObject createKey(Position position);
}
