package game.world;

import java.util.Set;

import game.objects.blocks.Block;
import game.objects.blocks.Door;
import game.objects.blocks.PickableObject;
import game.objects.entities.Enemy;
import game.objects.entities.PlayerImpl;
import game.utilities.Position;

/**
 * This interface declares a world initializer.
 */
public interface WorldInitializer {

    /**
     * Initializes initial free tiles.
     * 
     * @return free tiles
     */
    Set<Position> initializeFreeTiles();

    /**
     * Initializes all unbreakable blocks.
     * 
     * @return a set of unbreakable blocks
     */
    Set<Block> initializeUnbreakableBlocks();

    /**
     * Initializes all breakable blocks.
     * 
     * @param freeTiles tiles empty
     * @return a set of breakable blocks
     */
    Set<Block> initializeBreakableBlocks(Set<Position> freeTiles);

    /**
     * Initializes the key.
     * 
     * @param breakableBlocks breakable blocks where to get the position
     * @return the key
     */
    PickableObject initializeKey(Set<Block> breakableBlocks);

    /**
     * Initializes the door.
     * 
     * @param breakableBlocks breakable blocks where to get the position
     * @return the door
     */
    Door initializeDoor(Set<Block> breakableBlocks);

    /**
     * Initializes the player.
     * 
     * @param world where to add the player
     * @return the player
     */
    PlayerImpl initializePlayer(World world);

    /**
     * Initializes all enemies.
     * 
     * @param freeTiles tiles empty
     * @param world where to add the enemies
     * @return a set of enemies
     */
    Set<Enemy> initializeEnemies(Set<Position> freeTiles, World world);

}
