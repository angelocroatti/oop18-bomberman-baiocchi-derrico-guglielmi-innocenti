package game.world;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import game.graphics.SpriteSheet;
import game.maps.MapGenerator;
import game.maps.MapGeneratorImpl;
import game.objects.blocks.Block;
import game.objects.blocks.Door;
import game.objects.blocks.PickableObject;
import game.objects.entities.Enemy;
import game.objects.entities.PlayerImpl;
import game.theme.Theme;
import game.utilities.FrameSizeUtils;
import game.utilities.Position;

/**
 * This class models {@link WorldInitializer}.
 */
public class WorldInitializerImpl implements WorldInitializer {

    private static final int START_SAFE_ZONE = WorldImpl.LOWER_BOUND;
    private static final int END_SAFE_ZONE = 3;
    private static final int MAX_BREAKABLE_BLOCKS = FrameSizeUtils.NUM_TILES * 3;
    private static final int MIN_BREAKABLE_BLOCKS = FrameSizeUtils.NUM_TILES * 2;
    private static final int N_GHOST_ENEMY = 4;
    private static final int N_BALLOM_ENEMY = 4;
    private static final int N_SMART_ENEMY = 3;
    private static final int N_ENEMIES = N_GHOST_ENEMY + N_BALLOM_ENEMY + N_SMART_ENEMY;
    private final GameObjectFactory factory;
    private final MapGenerator map;

    /**
     * Creates {@code WorldInitializerImpl} that initialize world's objects.
     *
     * @param theme objects theme
     */
    public WorldInitializerImpl(final Theme theme) {
        this.factory = new GameObjectFactoryImpl(theme);
        this.map = new MapGeneratorImpl(this.factory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Position> initializeFreeTiles() {
        return this.map.getMapType().getFreePositions();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Block> initializeUnbreakableBlocks() {
        return this.map.getMapType().getMapLayout();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Block> initializeBreakableBlocks(final Set<Position> freeTiles) {
        final Random r = new Random();
        final int nBreakableBlocks = r.nextInt(MAX_BREAKABLE_BLOCKS - MIN_BREAKABLE_BLOCKS) + MIN_BREAKABLE_BLOCKS;
        final Set<Integer> positions = IntStream.range(0, nBreakableBlocks)
                .mapToObj(i -> r.nextInt(freeTiles.size() - this.getSafeZone().size()))
                .collect(Collectors.toSet());
        final Set<Block> breakableBlocks = this.getProtectiveBlocks();
        breakableBlocks.addAll(positions.stream()
                .map(i -> factory.createBreakableBlock(freeTiles.stream()
                        .filter(p -> !this.getSafeZone().stream().filter(q -> q.equals(p)).findFirst().isPresent())
                        .collect(Collectors.toList()).get(i)))
                .collect(Collectors.toSet()));
        return breakableBlocks;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PickableObject initializeKey(final Set<Block> breakableBlocks) {
        final Random r = new Random();
        return this.factory.createKey(breakableBlocks.stream()
                                                      .collect(Collectors.toList())
                                                      .get(r.nextInt(breakableBlocks.size()))
                                                      .getPosition());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Door initializeDoor(final Set<Block> breakableBlocks) {
        final Random r = new Random();
        return this.factory.createDoor(breakableBlocks.stream()
                                                      .map(b -> b.getPosition())
                                                      .collect(Collectors.toList())
                                                      .get(r.nextInt(breakableBlocks.size())));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlayerImpl initializePlayer(final World world) {
        return this.factory.createPlayer(new Position(START_SAFE_ZONE * SpriteSheet.SPRITE_SIZE_IN_GAME,
                    START_SAFE_ZONE * SpriteSheet.SPRITE_SIZE_IN_GAME), world);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Enemy> initializeEnemies(final Set<Position> freeTiles, final World world) {
        final Random r = new Random();
        final Set<Integer> positions = IntStream.range(0, N_ENEMIES)
                                        .mapToObj(n -> r.nextInt(freeTiles.size() - this.getSafeZone().size()))
                                        .collect(Collectors.toSet());
        final Set<Enemy> enemies = new HashSet<>();
        enemies.addAll(
                      positions.stream()
                               .limit(N_GHOST_ENEMY)
                               .map(i -> this.factory.createGhostEnemy(
                                                 freeTiles.stream()
                                                          .filter(p ->
                                                                !this.getSafeZone().stream()
                                                                                   .filter(q -> q.equals(p))
                                                                                   .findFirst().isPresent())
                                                               .collect(Collectors.toList())
                                                               .get(i), world))
                              .collect(Collectors.toSet()));
        enemies.addAll(
                      positions.stream()
                               .skip(N_GHOST_ENEMY)
                               .limit(N_BALLOM_ENEMY)
                               .map(i -> this.factory.createBalloomEnemy(
                                                 freeTiles.stream()
                                                           .filter(p ->
                                                               !this.getSafeZone().stream()
                                                                                  .filter(q -> q.equals(p))
                                                                                  .findFirst().isPresent())
                                                               .collect(Collectors.toList())
                                                               .get(i), world))
                               .collect(Collectors.toSet()));
        enemies.addAll(
                      positions.stream()
                               .skip(N_GHOST_ENEMY + N_BALLOM_ENEMY)
                               .map(i -> this.factory.createSmartEnemy(
                                                 freeTiles.stream()
                                                          .filter(p ->
                                                               !this.getSafeZone().stream()
                                                                                  .filter(q -> q.equals(p))
                                                                                  .findFirst().isPresent())
                                                               .collect(Collectors.toList())
                                                               .get(i), world))
                               .collect(Collectors.toSet()));
        return enemies;
    }

    private Set<Position> getSafeZone() {
        final Set<Position> safeZone = new HashSet<>();
        IntStream.range(START_SAFE_ZONE, END_SAFE_ZONE)
                .mapToObj(x -> IntStream.range(START_SAFE_ZONE, END_SAFE_ZONE).mapToObj(
                        y -> new Position(x * SpriteSheet.SPRITE_SIZE_IN_GAME, y * SpriteSheet.SPRITE_SIZE_IN_GAME))
                        .collect(Collectors.toSet()))
                .forEach(s -> safeZone.addAll(s));
        return safeZone;
    }

    private Set<Block> getProtectiveBlocks() {
        final Set<Block> blocks = new HashSet<>();
        blocks.add(this.factory.createBreakableBlock(
                   new Position(END_SAFE_ZONE * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                START_SAFE_ZONE * SpriteSheet.SPRITE_SIZE_IN_GAME)));
        blocks.add(this.factory.createBreakableBlock(
                new Position(START_SAFE_ZONE * SpriteSheet.SPRITE_SIZE_IN_GAME,
                             END_SAFE_ZONE * SpriteSheet.SPRITE_SIZE_IN_GAME)));
        return blocks;
    }
}
