package game.input.graph;

/**
 * This defines the possible colors of a node during a Graph visit.
 *
 */
public enum NodeColor {

    /**
     * White color indicates the node is not visited yet.
     */
    WHITE,
    /**
     * Grey color indicates the visit is started but not completed.
     */
    GREY,
    /**
     * Black color indicates the node has been visited yet.
     */
    BLACK;

}
