package game.input;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import game.graphics.SpriteSheet;
import game.objects.entities.DynamicObject;
import game.physics.Direction;

/**
 * This class models a {@link EnemyInputComponent}.
 * It randomly generates a new command.
 */
public class RandomInputComponent extends AbstractInputComponent implements EnemyInputComponent {

    private final List<Direction> directions;
    private int difficultyLvl = 1;
    private final double pixelToSeconds;
    private final Random randomizer;

    /**
     * Creates {@code RandomInputComponent}.
     *
     * @param en that command is given to.
     * @param distance covered by the enemy in a second.
     */
    public RandomInputComponent(final DynamicObject en, final double distance) {
        super(en);
        this.directions = Arrays.asList(Direction.values())
                                .stream()
                                .filter(d -> !d.equals(Direction.STOP))
                                .collect(Collectors.toList());
        this.pixelToSeconds = (distance * SpriteSheet.SPRITE_SIZE_IN_GAME) * difficultyLvl;
        this.randomizer = new Random();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getDifficultyLevel() {
        return this.difficultyLvl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void incrementDifficultyLevel() {
        this.difficultyLvl += 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void generateCommand() {
        final int directionsNumber = this.directions.size();
        final Direction dir = this.directions.get(this.randomizer.nextInt(directionsNumber));
        super.createDirectionCommand(dir, pixelToSeconds);
    }

}
