package game.graphics;

import java.awt.Graphics2D;

/**
 * This interface declares the graphic component of any GameObject.
 */
public interface GraphicsComponent {

    /**
     * Renders method for any component.
     *
     * @param g graphics to draw.
     */
    void render(Graphics2D g);
}
