package game.graphics;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import game.objects.blocks.Door;
import game.theme.DoorSprite;

/**
 * This class represents the door's graphic component and models {@link GraphicsComponent}.
 */
public class DoorGraphicComponent implements GraphicsComponent {

    private final Door door;
    private final DoorSprite doorSprite;

    /**
     * Creates a {@code DoorGraphicComponent}.
     * 
     * @param door the logic of the door
     * @param doorSprite door's sprite
     */
    public DoorGraphicComponent(final Door door, final DoorSprite doorSprite) {
        this.door = door;
        this.doorSprite = doorSprite;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(final Graphics2D g) {
        if (this.door.isOpen()) {
            g.drawImage(this.doorSprite.getOpenDoor().getImage(),
                    AffineTransform.getTranslateInstance(this.door.getPosition().getX(), this.door.getPosition().getY()),
                    null);
        } else {
            g.drawImage(this.doorSprite.getClosedDoor().getImage(),
                    AffineTransform.getTranslateInstance(this.door.getPosition().getX(), this.door.getPosition().getY()),
                    null);
        }
    }
}
