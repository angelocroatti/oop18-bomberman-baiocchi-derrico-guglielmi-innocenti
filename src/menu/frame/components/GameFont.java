package menu.frame.components;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class generates an instance designed to create a new {@link Font}. Use
 * getters to get the font created.
 */
public class GameFont {

    private static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    private static final int FONT_SCALE = 29;
    private static final String URL_FONT = "/MenuComponents/FontButton/Videophreak.ttf";

    private Font font;

    /**
     * Creates a {@code Font}.
     */
    public GameFont() {
        final InputStream is = this.getClass().getResourceAsStream(URL_FONT);
        try {
            this.font = Font.createFont(Font.TRUETYPE_FONT, is);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets a default {@link GameFont} with commons settings.
     * 
     * @return the {@link Font} with default settings
     */
    public Font getDefaultGameFont() {
        return getScaledGameFont(FONT_SCALE);
    }

    /**
     * Gets a default {@link GameFont} with commons settings and personal
     * {@link Font} size.
     * 
     * @param fontScale the real number to scale {@link Font} size
     * @return the {@link Font} with default settings with personal size
     */
    public Font getScaledGameFont(final double fontScale) {
        final long fontSize = Math.round(SCREEN_SIZE.getHeight() / fontScale);
        return this.font.deriveFont(Font.BOLD, fontSize);
    }

}
