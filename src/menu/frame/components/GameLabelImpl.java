package menu.frame.components;

import java.net.URL;

import javax.swing.Icon;

/**
 * This class extends {@link AbstractGameLabel} and should be implemented to
 * standardize the game design. In this specified case, it's a display area for
 * a externally loaded image ({@link GameImgIcon}).
 */
public class GameLabelImpl extends AbstractGameLabel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates a {@code GameLabelImpl} with no image and with an empty string for
     * the title.
     */
    public GameLabelImpl() {
        super();
    }

    /**
     * Creates a {@code GameLabelImpl} with string for the title.
     * 
     * @param title the text to be load
     */
    public GameLabelImpl(final String title) {
        super(title);
    }

    /**
     * Creates a {@code GameLabelImpl} with {@link Icon}.
     * 
     * @param icon the {@link Icon} image to be load.
     */
    public GameLabelImpl(final Icon icon) {
        super(icon);
    }

    /**
     * Creates a {@code GameLabelImpl} with {@link GameImgIcon} loaded from the
     * specified {@link URL} and resized using a specified real scalar.
     * 
     * @param imgURL the {@link URL} pointer to get image "resource"
     * @param scale the real scalar value to resize the image
     */
    public GameLabelImpl(final URL imgURL, final double scale) {
        super(imgURL, scale);
    }

}
