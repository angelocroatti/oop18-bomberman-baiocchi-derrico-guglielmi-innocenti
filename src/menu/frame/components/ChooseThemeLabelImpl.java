package menu.frame.components;

import javax.swing.JLabel;

import menu.frame.buttons.AbstractGameButton;

/**
 * This class extends from {@link GameLabelImpl} and it represents the relative sign
 * in theme menu.
 */
public class ChooseThemeLabelImpl extends AbstractGameLabel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String TITLE = "CHOOSE YOUR GAME THEME";

    /**
     * Creates {@code ChooseThemeLabelImpl} and sets the string to show, its font, and horizontal alignment.
     */
    public ChooseThemeLabelImpl() {
       super(TITLE);
       final GameFont font = new GameFont();
       super.setForeground(AbstractGameButton.TEXT_BUTTON_COLOR);
       super.setFont(font.getDefaultGameFont());
       super.setHorizontalAlignment(JLabel.CENTER);
    }

}
