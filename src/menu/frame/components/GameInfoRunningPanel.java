package menu.frame.components;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;

import game.graphics.SpriteSheet;
import game.world.World;

/**
 * This class models a {@link GamePanelImpl}.
 */
public class GameInfoRunningPanel extends AbstractGamePanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Panel's height.
     */
    public static final int PANEL_HEIGHT = SpriteSheet.SPRITE_SIZE_IN_GAME; 
    /**
     * Panel's width.
     */
    public static final int PANEL_WIDTH = WorldCanvasImpl.CANVAS_SIZE;
    private final World world;
    private final JLabel timer;
    private final JLabel points;
    private final JLabel key;

    /**
     * Creates a {@code GameInfoRunningPanel}.
     * 
     * @param world link to {@link World}
     */
    public GameInfoRunningPanel(final World world) {
        super(new GridLayout());
        this.world = world;
        this.timer = new GameLabelImpl();
        this.points = new GameLabelImpl();
        this.key = new GameLabelImpl();
        this.initializeLabels();
        this.disposeComponents();
    }

    /**
     * Sets the string of the timer.
     */
    public void render() {
        this.timer.setText(this.world.getGameTimer().toString());
        this.points.setText(this.world.getPlayerScore().getScore() + " points");
        if (!this.world.getKey().isPresent()) {
            this.key.setText("Key : true");
        }
    }

    private void initializeLabels() {
        this.timer.setText("00:00");
        this.points.setText("0 points");
        this.key.setText("Key : false");
    }

    private void disposeComponents() {
        this.add(this.timer);
        this.add(this.points);
        this.add(this.key);
        this.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
    }
}
