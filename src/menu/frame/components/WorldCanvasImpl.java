package menu.frame.components;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import game.graphics.Sprite;
import game.graphics.SpriteSheet;
import game.input.KeyInput;
import game.theme.Theme;
import game.utilities.FrameSizeUtils;
import game.utilities.Position;
import game.world.World;

/**
 * This class models {@link WorldCanvas}.
 */
public class WorldCanvasImpl extends Canvas implements WorldCanvas {

    /**
     * Canvas height and width.
     */
    public static final int CANVAS_SIZE = FrameSizeUtils.NUM_TILES * SpriteSheet.SPRITE_SIZE_IN_GAME;
    private static final long serialVersionUID = 1L;
    private final World world;
    private final Sprite grassSprite;
    private final Sprite unbreakableBlock;

    /**
     * Creates a {@code WorldCanvasImpl} that draw on screen the world's objects.
     * 
     * @param world the link to the world containers
     * @param theme the theme of the sprite
     */
    public WorldCanvasImpl(final World world, final Theme theme) {
        super();
        this.world = world;
        this.grassSprite = theme.getSprites().getFloorSprite();
        this.unbreakableBlock = theme.getSprites().getWallSprite();
        super.setPreferredSize(new Dimension(CANVAS_SIZE, CANVAS_SIZE));
        super.addKeyListener(new KeyInput(world.getPlayer(), world.getGameContext()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render() {
        final BufferStrategy bs = this.getBufferStrategy();
        if (bs == null) {
            this.createBufferStrategy(3);
            return;
        }
        final Graphics2D g = (Graphics2D) bs.getDrawGraphics();
        this.renderGrass(g);
        this.renderPerimeterBlocks(g);
        this.renderAllObjects(g);
        g.dispose();
        bs.show();
    }

    private void renderAllObjects(final Graphics2D g) {
        this.world.getAllGameObjects().forEach(o -> o.render(g));
    }

    private void renderGrass(final Graphics2D g) {
        IntStream.range(0, FrameSizeUtils.NUM_TILES)
                 .mapToObj(x -> IntStream.range(0, FrameSizeUtils.NUM_TILES)
                                         .mapToObj(y -> new Position(x * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                     y * SpriteSheet.SPRITE_SIZE_IN_GAME))
                                         .collect(Collectors.toSet()))
                 .forEach(s -> s.forEach(p -> g.drawImage(this.grassSprite.getImage(),
                                                 AffineTransform.getTranslateInstance(p.getX(), p.getY()), null)));
    }

    private void renderPerimeterBlocks(final Graphics2D g) {
        this.leftBound(g);
        this.topBound(g);
        this.lowerBound(g);
        this.rightBound(g);
    }

    private void leftBound(final Graphics2D g) {
        IntStream.of(0)
                 .mapToObj(x -> IntStream.range(0, FrameSizeUtils.NUM_TILES)
                                         .mapToObj(y -> new Position(x * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                     y * SpriteSheet.SPRITE_SIZE_IN_GAME))
                                         .collect(Collectors.toSet()))
                 .forEach(s -> s.forEach(p -> g.drawImage(this.unbreakableBlock.getImage(),
                                                 AffineTransform.getTranslateInstance(p.getX(), p.getY()), null)));
    }

    private void rightBound(final Graphics2D g) {
        IntStream.of(FrameSizeUtils.NUM_TILES - 1)
                 .mapToObj(x -> IntStream.range(0, FrameSizeUtils.NUM_TILES - 1)
                                         .mapToObj(y -> new Position(x * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                     y * SpriteSheet.SPRITE_SIZE_IN_GAME))
                                         .collect(Collectors.toSet()))
                 .forEach(s -> s.forEach(p -> g.drawImage(this.unbreakableBlock.getImage(),
                                                 AffineTransform.getTranslateInstance(p.getX(), p.getY()), null)));
    }

    private void topBound(final Graphics2D g) {
        IntStream.of(0)
                 .mapToObj(y -> IntStream.range(0, FrameSizeUtils.NUM_TILES)
                                         .mapToObj(x -> new Position(x * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                     y * SpriteSheet.SPRITE_SIZE_IN_GAME))
                                         .collect(Collectors.toSet()))
                 .forEach(s -> s.forEach(p -> g.drawImage(this.unbreakableBlock.getImage(),
                                                 AffineTransform.getTranslateInstance(p.getX(), p.getY()), null)));
    }

    private void lowerBound(final Graphics2D g) {
        IntStream.of(FrameSizeUtils.NUM_TILES - 1)
                 .mapToObj(y -> IntStream.range(0, FrameSizeUtils.NUM_TILES)
                                         .mapToObj(x -> new Position(x * SpriteSheet.SPRITE_SIZE_IN_GAME,
                                                                     y * SpriteSheet.SPRITE_SIZE_IN_GAME))
                                         .collect(Collectors.toSet()))
                 .forEach(s -> s.forEach(p -> g.drawImage(this.unbreakableBlock.getImage(),
                                         AffineTransform.getTranslateInstance(p.getX(), p.getY()), null)));
    }
}
