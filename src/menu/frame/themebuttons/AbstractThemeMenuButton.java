package menu.frame.themebuttons;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;

import menu.frame.buttons.AbstractGameButton;
import menu.mouselisteners.ThemeMouseListener;
import sounds.SoundImpl;

/**
 * Abstract class that sets all parameters for the theme menu button.
 */
public abstract class AbstractThemeMenuButton extends AbstractGameButton {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final String FOLDER = "/ChooseThemeMenu";

    /**
     * Creates {@code AbstractThemeMenuButton} that sets the button with its
     * image, size, {@link ThemeMouseListener} and actionListener.
     */
    public AbstractThemeMenuButton() {
        super();
        this.setBorderPainted(false);
        this.addActionListener(a -> this.chooseTheme());
    }

    /**
     * Adds a new mouseListener to the button.
     *
     * @param select image is shown when mouse is over the button
     */
    public void loadMouseListener(final ImageIcon select) {
        try {
            this.addMouseListener(new ThemeMouseListener(new SoundImpl(FOLDER + "/select.wav"), this, select));
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Selects the theme to load in a new game.
     */
    public abstract void chooseTheme();

}
