package menu.frame;

import java.awt.Image;
import java.net.URL;

import game.state.GameContext;
import menu.frame.components.GameImgIcon;

/**
 * This is the game over menu where you can choose two different choices: main
 * menu or exits. This class is designed to automatically scale down based of
 * the main screen size. It implements specific game components that make
 * standard the overall design.
 */
public class GameMenuGameOver extends AbstractGameMenu {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final String IMAGE_PATH = "MenuComponents/Images/GameOverMenuBackground/";
    private static final URL IMG_URL_GAME_OVER = ClassLoader.getSystemResource(IMAGE_PATH + "GameOver.gif");
    private static final double IMG_SCALE = 0.6;

    /**
     * Creates a {@code GameMenuGameOver} frame with default settings.
     * 
     * @param gameContext the {@link GameContext} handler
     */
    public GameMenuGameOver(final GameContext gameContext) {
        super(gameContext, new GameImgIcon(IMG_URL_GAME_OVER, IMG_SCALE, Image.SCALE_DEFAULT));
    }

}
