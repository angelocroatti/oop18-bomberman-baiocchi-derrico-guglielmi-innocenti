package menu.frame.buttons;

/**
 * This class extends {@link AbstractGameButton}. It allows easy management of
 * the commons behaviors and settings of the Exit button in case of one or
 * multiple uses within its own program.
 */
public class ExitButton extends AbstractGameButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final String TEXT_BUTTON = "EXIT";

    /**
     * Creates a {@code ExitButton}.
     */
    public ExitButton() {
        super(TEXT_BUTTON);
        super.addActionListener(l -> System.exit(0));
    }

}
