package menu.frame.buttons;

import game.state.GameContext;

/**
 * This class extends {@link AbstractGameButton}. It allows easy management of
 * the commons behaviors and settings of the Info button in case of one or
 */
public class InfoButton extends AbstractGameButton {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final String TEXT_BUTTON = "INFO";

    /**
     * Creates a {@code InfoButton}.
     * 
     * @param gameContext the {@link GameContext} handler
     */
    public InfoButton(final GameContext gameContext) {
        super(TEXT_BUTTON);
        super.addActionListener(l -> gameContext.requestInfoState());
    }

}
