package menu.mouselisteners;

import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import sounds.Sound;

/**
 * Mouse listener to add in buttons use in choose-theme menu window.
 */
public class ThemeMouseListener extends AbstractMouseListener {

    private final JButton button;
    private final ImageIcon noSelectionImage;
    private final ImageIcon selectionImage;

    /**
     * Creates {@code ThemeMouseListener}.
     *
     * @param sound to play when mouse is on button
     * @param button reference
     * @param image to show when mouse is on button
     */
    public ThemeMouseListener(final Sound sound, final JButton button, final ImageIcon image) {
        super(sound);
        this.button = button;
        this.selectionImage = image;
        if (this.button.getIcon() instanceof ImageIcon) {
            this.noSelectionImage = (ImageIcon) this.button.getIcon();
        } else {
            throw new ClassCastException();
        }
    }

    /**
     * Plays the sound and changes the image when mouse is on the button.
     */
    @Override
    public void mouseEntered(final MouseEvent e) {
        this.button.setIcon(this.selectionImage);
    }

    /**
     * Resets the initial image when mouse isn't on the button anymore.
     */
    @Override
    public void mouseExited(final MouseEvent e) {
        this.button.setIcon(this.noSelectionImage);
    }

    /**
     * Resets the initial image when mouse click is released.
     */
    @Override
    public void mouseReleased(final MouseEvent e) {
        this.button.setIcon(this.noSelectionImage);
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
    }

    @Override
    public void mousePressed(final MouseEvent e) {
    }

}
